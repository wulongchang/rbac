package com.cmbt.dao.impl;

import com.cmbt.bean.User;
import com.cmbt.dao.api.UserDao;
import junit.framework.TestCase;

public class UserDaoImplTest extends TestCase {
    UserDao userDao = new UserDaoImpl();

    public void testSelectAllUserList() {

    }

    public void testSelectUserByUserAcct() {
        User user = userDao.selectUserByUserAcct("admin");
        System.out.println(user);
    }

    public void testInsertUser() {
        User user = new User();
        user.setPhone("123123");
        user.setUserAcct("test");
        user.setGender("女");
        user.setEmail("234234@qq.com");
        user.setAge(88);
        user.setPassword("123123");
        user.setUsername("test111");
        userDao.insertUser(user);
    }

    public void testTestInsertUser() {
        for (int i = 0; i < 50; i++) {
            User user = new User(null, "userAcct" + i, "123123", "username" + i, i, i + "qq.com", "123123" + i, "女", "");
            userDao.insertUser(user);
        }

    }
}
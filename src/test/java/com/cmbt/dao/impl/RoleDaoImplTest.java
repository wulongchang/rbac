package com.cmbt.dao.impl;

import com.cmbt.bean.Role;
import junit.framework.TestCase;

import java.util.List;

public class RoleDaoImplTest extends TestCase {
    private RoleDaoImpl roleDaoImpl= new RoleDaoImpl();

    public void testSelectRoleById() {
        Role role = roleDaoImpl.selectRoleById(1);
        System.out.println(role);
    }

    public void testSelectPermissionIdsByRoleId() {
//        List<Integer> list = roleDaoImpl.selectPermissionIdsByRoleId(1);
//        System.out.println(list);
    }

    public void testSelectAllRoleList() {
        List<Role> roleList = roleDaoImpl.selectAllRoleList();
        System.out.println(roleList);
    }

//    public void testSelectRoleById() {
//        Role role = roleDaoImpl.selectRoleById(3);
//        System.out.println(role);
//
//    }
}
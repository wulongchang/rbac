package com.cmbt.service.impl;

import com.cmbt.bean.Role;
import com.cmbt.bean.User;
import com.cmbt.dao.api.UserDao;
import com.cmbt.dao.impl.UserDaoImpl;
import com.cmbt.service.api.UserService;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImplTest extends TestCase {
    UserService userService = new UserServiceImpl();
    RoleServiceImpl roleService=new RoleServiceImpl();

    @Test
    public void testGetUserList() {
        List<User> allUserList = userService.getAllUserList();
        for (User user : allUserList){
            System.out.println(user);
        }
    }

    public void testGetAllUserList() {
    }

    public void testLogin() {
        User user = userService.login("admin", "123123");
        System.out.println(user);
    }

    public void testUpdate(){
        User user = new User();
        user.setUserAcct("zd");
        user.setUsername("zdzd");
        user.setPassword("1122");
        user.setEmail("aaa@111");
        user.setAge(13);
        user.setPhone("1331313");
        user.setGender("男");
        userService.updateUser(user);

    }
    public void testInsert(){
        User user = new User();
        user.setUserAcct("测试用户角色关系");
        user.setUsername("zdzd");
        user.setPassword("1122");
        user.setEmail("aaa@111");
        user.setAge(13);
        user.setPhone("1331313");
        user.setGender("男");

        Role role1 = new Role();
        role1.setRoleId(1);
        role1.setRoleName("超级管理员");
        Role role2=new Role();
        role2.setRoleId(2);
        role2.setRoleName("业务员");
        List<Role> roleList=new ArrayList<>();


        //user.setRole(roleList);
        userService.insertUser(user);

    }

    public void testSelect(){
        System.out.println(roleService.selectAllRoleList());
    }

    public void testInsert2(){
        User user = new User();
       user.setUserAcct("测试用户角色关系5");
       user.setUsername("zdzd");
       user.setPassword("1122");
       user.setEmail("aaa@111");
       user.setAge(13);
       user.setPhone("1331313");
       user.setGender("男");
        User user1 = userService.insertUser(user);
        int[] ints = {1,2};
        userService.insertUserRole(user1,ints);
    }





}

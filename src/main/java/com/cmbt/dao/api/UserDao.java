package com.cmbt.dao.api;

import com.cmbt.bean.Page;
import com.cmbt.bean.User;

import java.util.List;

public interface UserDao {
    List<User> selectAllUserList();

    User selectUserByUserAcct(String userAcct);


    void insertUserRole(User user,int[] roleList);
    public void deleteUserRole(User user, int[] roleList);
    User insertUser(User user);

    User updateUser(User user);

//分页展示和查询
    List<User> selectUserListByPage(Page pg, String keyword);


    int selectTotalPage(int pageSize,String keyword);


    void deleteUserById(Integer userId);

    void deleteUserAndRoleByUserId(Integer userId);
}

package com.cmbt.dao.api;

import com.cmbt.bean.Page;
import com.cmbt.bean.Role;

import java.util.List;

public interface RoleDao {

    List<Role> selectAllRoleList();

    Role selectRoleById(Integer roleId);

    int selectTotalPage(int pageSize,String keyword);
    List<Role> selectRoleListByPage(Page pg,String keyword);



    int addRole(Role role);

    int addRoleAndPermission(int roleId,int permissionId);

    List<Integer> selectPermissionIdsByRoleId(int roleId);

    int modifyRole(Role role);

    void deleteRoleAndPermission(int roleId);

    void deleteRoleById(Integer roleId);

    List<Integer> selectRoleIdByUserId(int userId);

    void deleteUserAndRoleByRoleId(Integer roleId);
}


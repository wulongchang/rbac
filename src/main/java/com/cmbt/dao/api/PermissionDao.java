package com.cmbt.dao.api;

import com.cmbt.bean.Permission;

import java.util.List;

public interface PermissionDao {
    List<Permission> selectPermissionList();
}

package com.cmbt.dao.impl;

import com.cmbt.bean.Page;
import com.cmbt.bean.User;
import com.cmbt.dao.api.UserDao;
import com.cmbt.utils.DruidJdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    // commons-dbutils的操作对象
    QueryRunner qr = new QueryRunner(DruidJdbcUtils.getDataSource());

    @Override
    public List<User> selectAllUserList() {
        try {
            String sql = "select user_id as userId,username,password,age,email from t_user";
            List<User> list = qr.query(sql, new BeanListHandler<>(User.class));
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public User selectUserByUserAcct(String userAcct) {
            String sql = "SELECT user_id as userId,user_acct as userAcct,password,username,age,email,phone,gender FROM t_user WHERE user_acct = ?";
            try {
                User user = qr.query(sql, new BeanHandler<>(User.class), userAcct);
                return user;
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return null;
    }

    @Override
    public void deleteUserById(Integer userId) {
        String sql = "delete from t_user where user_id= ?";
        try {
            qr.update(sql,userId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void deleteUserAndRoleByUserId(Integer userId) {
        String sql = "delete from t_user_role where user_id= ?";
        try {
            qr.update(sql,userId);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //新增用户
    @Override
    public User insertUser(User user) {
        String sql = "insert  into t_user(user_acct,password,username,age,email,phone,gender) values(?,?,?,?,?,?,?)";
        String sql1 = "SELECT user_id as userId,user_acct as userAcct,password,username,age,email,phone,gender FROM t_user WHERE user_acct = ?";
        try {
             qr.insert(sql, new BeanHandler<>(User.class), user.getUserAcct(), user.getPassword(), user.getUsername(), user.getAge(),
                    user.getEmail(), user.getPhone(), user.getGender());
             User user1 = qr.query(sql1, new BeanHandler<User>(User.class), user.getUserAcct());
            return user1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }



    //新增用户中的角色
    @Override
    public void insertUserRole(User user,int[] roleList) {
        String sql="insert into t_user_role values(?,?)";
        for (int i=0;i<roleList.length;i++){
            try {
                qr.update(sql,user.getUserId(),roleList[i]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //删除用户中的角色
    @Override
    public void deleteUserRole(User user, int[] roleList) {
        String sql="delete from t_user_role where role_id =?";
        for (int i=0;i<roleList.length;i++){
            try {
                qr.update(sql,roleList[i]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User updateUser(User user) {
        String sql = "update t_user set password=?,username=?,age=?,email=?,phone=?,gender=?where user_acct=?";
        String sql1 = "SELECT user_id as userId,user_acct as userAcct,password,username,age,email,phone,gender FROM t_user WHERE user_acct = ?";
        Object[] params = {user.getPassword(), user.getUsername(), user.getAge(), user.getEmail(), user.getPhone(), user.getGender(), user.getUserAcct()};
        try {
            qr.update(sql, params);
            User user1 = qr.query(sql1, new BeanHandler<User>(User.class), user.getUserAcct());
            return user1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    //分页展示和查询
    public Connection con = null;
    public PreparedStatement pst = null;
    public Statement sm = null;
    public ResultSet rs = null;

    @Override
    public List<User> selectUserListByPage(Page page,String keyword) {
        String sql1 = "SELECT users.user_id as userId, user_acct as userAcct,password, username, age, email, phone, gender, roles as roleName " +
                "FROM t_user users LEFT JOIN (SELECT user_id, GROUP_CONCAT(role_name) roles FROM t_user_role a " +
                "LEFT JOIN t_role b ON a.role_id=b.role_id GROUP BY user_id) uroles ON users.user_id=uroles.user_id " +
                "where user_acct like ? limit ?,? ";  //分页查询
        int pageSize = page.getPageSize();
            // 从哪start这一条开始
            int start = (page.getCurrentPage() - 1) * pageSize;

            List<User> userList = null;
            try {
                keyword = "%" + keyword + "%";
                userList = qr.query(sql1, new BeanListHandler<User>(User.class),keyword, start,pageSize);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return userList;
    }




    @Override
    public int selectTotalPage(int pageSize,String keyword) {
        int total=0,totalPage=0;
        String sql="select count(user_id) from t_user where user_acct like ?";
        try {
            keyword = "%" + keyword + "%";
            Object obj=qr.query(sql,new ScalarHandler(),keyword);  //总数
            total = Integer.parseInt(String.valueOf(obj));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        totalPage=total%pageSize>0?total/pageSize+1:total/pageSize;  //总页数

        return totalPage;
    }
}

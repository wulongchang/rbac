package com.cmbt.dao.impl;

import com.cmbt.bean.Permission;
import com.cmbt.dao.api.PermissionDao;
import com.cmbt.utils.DruidJdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class PermissionDaoImpl implements PermissionDao {
    QueryRunner qr = new QueryRunner(DruidJdbcUtils.getDataSource());
    @Override
    public List<Permission> selectPermissionList() {
        try {
            String sql = "select permission_id as permissionId,permission_title as permissionTitle from t_permission";
            List<Permission> list = qr.query(sql, new BeanListHandler<>(Permission.class));
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}

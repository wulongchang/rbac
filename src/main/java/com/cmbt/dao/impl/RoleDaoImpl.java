package com.cmbt.dao.impl;

import com.cmbt.bean.Page;
import com.cmbt.bean.Role;
import com.cmbt.dao.api.RoleDao;
import com.cmbt.utils.DruidJdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleDaoImpl implements RoleDao {

    // commons-dbutils的操作对象
    QueryRunner qr = new QueryRunner(DruidJdbcUtils.getDataSource());

    @Override
    public List<Role> selectAllRoleList() {
        try {
            String sql = "select role_id as roleId,role_name as roleName,remark from t_role";
            List<Role> list = qr.query(sql, new BeanListHandler<>(Role.class));
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Role selectRoleById(Integer roleId) {
        String sql = "SELECT role_id as roleId,role_name as roleName,remark FROM t_role WHERE role_id = ?";
        try {
            Role role = qr.query(sql, new BeanHandler<>(Role.class), roleId);
            return role;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Role> selectRoleListByPage(Page page,String keyword) {
        String sql1 = "SELECT roles.role_id as roleId, role_name as roleName, remark, permissions " +
                "FROM t_role roles LEFT JOIN (SELECT role_id, GROUP_CONCAT(permission_title) permissions " +
                "FROM t_role_permission a LEFT JOIN t_permission b ON a.permission_id=b.permission_id GROUP BY role_id) rpermissions " +
                "ON roles.role_id=rpermissions.role_id where role_name like ? limit ?,?";  //分页查询
        int pageSize = page.getPageSize();

        // 从哪start这一条开始
        int start = (page.getCurrentPage() - 1) * pageSize;

        List<Role> roleList = null;
        try {
            keyword = "%" + keyword + "%";
            roleList = qr.query(sql1, new BeanListHandler<Role>(Role.class), keyword,start,pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roleList;


    }



    @Override
    public int selectTotalPage(int pageSize,String keyword) {
        int total=0,totalPage=0;
        String sql="select count(role_id) from t_role where role_name like ?";
        try {
            keyword = "%" + keyword + "%";
            Object obj=qr.query(sql,new ScalarHandler(),keyword);  //总数
            total = Integer.parseInt(String.valueOf(obj));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        totalPage=total%pageSize>0?total/pageSize+1:total/pageSize;  //总页数

        return totalPage;
    }



    @Override
    public int addRole(Role role) {

        int i=0;
        try {
            String sql = "insert into t_role(role_name,remark)values(?,?)";
            BigInteger object = qr.insert(sql, new ScalarHandler<>(), role.getRoleName(), role.getRemark());
            i = object.intValue();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;


    }

    @Override
    public int modifyRole(Role role) {
        int i = 0;
        try {
            String sql = "update t_role set role_name=?,remark=?where role_id=?";
            Object[] params = {role.getRoleName(),role.getRemark(),role.getRoleId()};
            i = qr.update(sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public void deleteRoleAndPermission(int roleId) {
        String sql ="delete from t_role_permission where role_id = ?";
        try {
             qr.update(sql, roleId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteRoleById(Integer roleId) {
        // 删除角色
        String sql ="delete from t_role where role_id = ?";
        try {
            qr.update(sql, roleId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int addRoleAndPermission(int roleId, int permissionId) {
        int i = 0;
        try {
            String sql = "insert into t_role_permission(role_id,permission_id)values(?,?)";

            i = qr.update(sql, roleId, permissionId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;

    }

    @Override
    public List<Integer> selectPermissionIdsByRoleId(int roleId) {
        List<Integer> mids = new ArrayList<>();
        try {
            String sql = "select permission_id from t_role_permission where role_id=?";
            //自定义结果集处理器
            ResultSetHandler<List<Integer>> resultSetHandler = new ResultSetHandler<List<Integer>>(){
                @Override
                public List<Integer> handle(ResultSet resultSet) throws SQLException {
                    List<Integer> list = new ArrayList<>();
                    while (resultSet.next()){
                        int mid = resultSet.getInt("permission_id");
                        list.add(mid);
                    }
                    return list;
                }
            };
            mids = qr.query(sql,resultSetHandler,roleId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mids;

    }
    @Override
    public List<Integer> selectRoleIdByUserId(int userId) {
        List<Integer> mids = new ArrayList<>();
        try {
            String sql = "select role_id from t_user_role where user_id=?";
            //自定义结果集处理器
            ResultSetHandler<List<Integer>> resultSetHandler = new ResultSetHandler<List<Integer>>(){
                @Override
                public List<Integer> handle(ResultSet resultSet) throws SQLException {
                    List<Integer> list = new ArrayList<>();
                    while (resultSet.next()){
                        int mid = resultSet.getInt("role_id");
                        list.add(mid);
                    }
                    return list;
                }
            };
            mids = qr.query(sql,resultSetHandler,userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mids;

    }

    @Override
    public void deleteUserAndRoleByRoleId(Integer roleId) {
        // 删除用户与角色的对应关系
        String sql = "delete from t_user_role where role_id = ?";
        try {
            qr.update(sql,roleId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

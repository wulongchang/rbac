package com.cmbt.utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DruidJdbcUtils {
    private static DataSource dataSource = null;

    static {
        Properties prop = new Properties();
        try {
            InputStream inputStream = DruidJdbcUtils.class.getClassLoader().getResourceAsStream("druid.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            try {
                dataSource=DruidDataSourceFactory.createDataSource(properties);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // 获取连接池对象
    public static DataSource getDataSource(){
        return dataSource;
    }
}

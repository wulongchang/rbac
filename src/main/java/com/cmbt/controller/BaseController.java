package com.cmbt.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * 所有Controller在写之前一定要先继承此类，其中做了防乱码处理；浏览器以Json的形势解析返回值
 * 逻辑是
 *      1、获取请求中的method属性的值，依据它去确定什么方法来当前处理请求
 *      2、通过反射找到该方法进行调用
 *      3、method = xxx ，与controller中的方法名xxx一定要一致
 */
public class BaseController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 解决请求乱码问题
        req.setCharacterEncoding("UTF-8");
        // 解决响应乱码问题
        resp.setCharacterEncoding("UTF-8");
        //  告诉客户端以json的形式解码
//        resp.setHeader("content-type","text/json;charset=UTF-8");
        // 1、获得方法名称
        String methodName = req.getParameter("function");
        Method method = null;

        // 2、通过方法名和方法所需要的参数获得Method对象
        try {
            //  获取当前类的字节码文件
            Class<? extends BaseController> clazz = this.getClass();
            /**
             * clazz.getMethod();
             * 查找当前类，有没有method值所对应的处理方法，它相当于:
             * if(method.equals("add")){}
             */
            method = clazz.getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
        } catch (Exception e) {
            throw new RuntimeException("调用的方法：" + methodName + "不存在", e);
        }

        // 3、通过Method对象调用方法
        try {
            method.invoke(this, req, resp);
//            if (req != null && result.trim().length() > 0) {// 如果返回的result不为空
//                int index = result.indexOf(":");// 获得第一个冒号的位置
//                if (index == -1) {// 如果没有冒号，就使用转发
//                    req.getRequestDispatcher(result).forward(req, resp);
//                } else {// 如果有冒号
//                    String start = result.substring(0, index);// 截取前缀
//                    String path = result.substring(index + 1);// 截取路径
//                    if (start.equalsIgnoreCase("f")) {// 前缀为f表示使用转发
//                        req.getRequestDispatcher(path).forward(req, resp);
//                    } else if (start.equalsIgnoreCase("r")) {// 前缀为r表示使用重定向
//                        resp.sendRedirect(req.getContextPath() + path);
//                    }
//                }
//            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

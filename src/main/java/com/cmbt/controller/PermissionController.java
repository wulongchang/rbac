package com.cmbt.controller;

import com.alibaba.fastjson.JSON;
import com.cmbt.bean.Permission;
import com.cmbt.bean.ResultEntity;
import com.cmbt.service.api.PermissionService;
import com.cmbt.service.impl.PermissionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class PermissionController extends BaseController {
    PermissionService permissionService = new PermissionServiceImpl();

    public void perList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String toPage = req.getParameter("toPage");
//        req.setAttribute("isFirst",false);

//        req.setAttribute("permissionList", permissionList);
//        req.getRequestDispatcher(toPage).forward(req, resp);
        List<Permission> permissionList = permissionService.getPermissionList();
        if (permissionList != null) {
            ResultEntity<List<Permission>> listResultEntity = ResultEntity.successWithData(permissionList);
            String s = JSON.toJSONString(listResultEntity);
            resp.getWriter().write(s);
        } else {
            // 如果不存在则返回json数据提示错误信息
            ResultEntity<String> failed = ResultEntity.failed("账号不存在或账号密码错误");
            String json = JSON.toJSONString(failed);
            resp.getWriter().write(json);

        }
    }
    public void perList2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String toPage = req.getParameter("toPage");
       req.setAttribute("isFirst",false);
        List<Permission> permissionList = permissionService.getPermissionList();
       req.setAttribute("permissionList", permissionList);
       req.getRequestDispatcher(toPage).forward(req, resp);

    }
}
package com.cmbt.controller;

import com.alibaba.fastjson.JSON;
import com.cmbt.bean.Page;
import com.cmbt.bean.Permission;
import com.cmbt.bean.ResultEntity;
import com.cmbt.bean.Role;
import com.cmbt.service.api.RoleService;
import com.cmbt.service.impl.RoleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RoleController extends BaseController{
    RoleService roleService = new RoleServiceImpl();
    //分页展示
    public void rolePageServlet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        //获取模糊查询数据
        String keyword = "";
        keyword = request.getParameter("keyword");
        if(keyword == null){
            keyword = "";
        }
        request.setAttribute("keyword",keyword);
        Object keyword1 = request.getAttribute("keyword");

        //获取当前页数，首次进入时currentPage为1，点击超链接时获取currentPage页数
        int currentPage = request.getParameter("currentPage") == null ? 1 : Integer.parseInt(request.getParameter("currentPage"));


        // 2、计算从哪里开始
        //通过page实现类获得总页数，这里的16为每页数目
        int pageSize = 16;
        int totalPage = roleService.getTotalPage(pageSize,keyword);



        // 3、前一页页数、后一页变量值
        int prePage = currentPage - 1 > 0 ? currentPage - 1 : currentPage;
        int nextPage = currentPage + 1 < totalPage ? currentPage + 1 : totalPage;


        //使用request.setAttribute方法便于页面中使用el语法
        request.setAttribute("totalPage", totalPage);
        request.setAttribute("prePage", prePage);
        request.setAttribute("nextPage", nextPage);
        request.setAttribute("isFirst",false);
        request.setAttribute("currentPage",currentPage);
        //获得当前页的数据
        Page page = new Page();
        page.setCurrentPage(currentPage);
        page.setPageSize(pageSize);

        List<Role> roleList = roleService.queryUserByPage(page,keyword);
        request.setAttribute("roleList", roleList);
        request.setAttribute("currentPage", currentPage);
        request.getRequestDispatcher("role.jsp").forward(request, response);
    }

    public void addRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String roleName = req.getParameter("roleName");

        String remark = req.getParameter("remark");
        Role role = new Role(null, roleName, remark,"");

        String[] permissionIDS = req.getParameterValues("permissionID[]");

        boolean p = roleService.addRole(role, permissionIDS);

        if(p){
            ResultEntity<Object> resultEntity = ResultEntity.successWithoutData();
           String json = JSON.toJSONString(resultEntity);

            resp.getWriter().write(json);
        }else {
            // 返回的false说明添加失败，返回错误消息
            ResultEntity<Object> resultEntity = ResultEntity.failed("增加失败！");
            String json = JSON.toJSONString(resultEntity);
            resp.getWriter().write(json);
        }


    }

    public void queryRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //接受角色信息queryRole
        String rid = req.getParameter("roleId");
        int roleId = rid==null?0:Integer.parseInt(rid);
        //根据角色信息ID查询角色的原始信息
        Role role  = roleService.getRoleById(roleId);

        //查询当前角色拥有的权限的id
        List<Integer> permissionIds = roleService.getPermissionIdsByRoleId(roleId);

        if(permissionIds != null){
            ResultEntity<List<Integer>> listResultEntity = ResultEntity.successWithData(permissionIds);
            String s = JSON.toJSONString(listResultEntity);
            resp.getWriter().write(s);
        }else {
            // 如果不存在则返回json数据提示错误信息
            ResultEntity<String> failed = ResultEntity.failed("账号不存在或账号密码错误");
            String json = JSON.toJSONString(failed);
            resp.getWriter().write(json);
        }

    }

   public void modifyRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收修改后的角色信息

       Integer roleId = Integer.valueOf(req.getParameter("roleId"));
       String roleName = req.getParameter("roleName");
       String remark = req.getParameter("remark");
       Role role = new Role(roleId,roleName,remark,"");

       //获取传递过来的的选择的权限ID
       String[] permissionIDS = req.getParameterValues("permissionID[]");
       //执行修改
       boolean x = roleService.updateRole(role, permissionIDS);

       if(x){
           ResultEntity<Object> resultEntity = ResultEntity.successWithoutData();
           String json = JSON.toJSONString(resultEntity);

           resp.getWriter().write(json);
       }else {
           // 返回的false说明添加失败，返回错误消息
           ResultEntity<Object> resultEntity = ResultEntity.failed("修改失败！");
           String json = JSON.toJSONString(resultEntity);
           resp.getWriter().write(json);
       }
   }

    public void removeRoles(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        // 获取要删除的用户id数组
        String[] roleIdsForStringArray = req.getParameterValues("roleIds[]");
        // 将数组存在list中
        ArrayList<Integer> roleIds = new ArrayList<>();
        for(int i=0;i< roleIdsForStringArray.length;i++) {
            int userId = Integer.parseInt(roleIdsForStringArray[i]);
            roleIds.add(userId);
        }
        // 执行删除操作
        roleService.removeRoles(roleIds);
        // 返回成功信息
        ResultEntity<Object> resultEntity = ResultEntity.successWithoutData();
        String json = JSON.toJSONString(resultEntity);
        resp.getWriter().write(json);
    }





    //查询所有角色跳转到用户页面
    public void getRoles(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        request.setAttribute("isFirst",false);
        List<Role> roleList = roleService.selectAllRoleList();
        request.setAttribute("roleList",roleList);
        /*request.getRequestDispatcher("add.jsp").forward(request, response);*/

        request.getRequestDispatcher("add.jsp").forward(request,response);

    }

    //查询所有角色跳转到编辑页面
    public void getRolesEdit(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        List<Role> roleList = roleService.selectAllRoleList();

        if (roleList != null) {
            ResultEntity<List<Role>> listResultEntity = ResultEntity.successWithData(roleList);
            String s = JSON.toJSONString(listResultEntity);
            response.getWriter().write(s);
        } else {
            // 如果不存在则返回json数据提示错误信息
            ResultEntity<String> failed = ResultEntity.failed("账号不存在或账号密码错误");
            String json = JSON.toJSONString(failed);
            response.getWriter().write(json);

        }
    }

    public void getUserRoles(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String userId = request.getParameter("userId");
        int roleIdTemp=Integer.parseInt(userId);
        List<Integer> roleIds = roleService.selectRoleIdByUserId(roleIdTemp);
        if(roleIds != null){
            ResultEntity<List<Integer>> listResultEntity = ResultEntity.successWithData(roleIds);
            String s = JSON.toJSONString(listResultEntity);
            response.getWriter().write(s);
        }else {
            // 如果不存在则返回json数据提示错误信息
            ResultEntity<String> failed = ResultEntity.failed("账号不存在或账号密码错误");
            String json = JSON.toJSONString(failed);
            response.getWriter().write(json);
        }
    }



}

package com.cmbt.controller;

import com.alibaba.fastjson.JSON;
import com.cmbt.bean.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 示例Controller
 */
public class TestController extends BaseController{
    /**
     * 测试方法，仅供参考使用
     * 请求地址：http://localhost:8080/rbac/testController?function=test
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void test(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        user.setUsername("张三");
        user.setEmail("2222@qq.com");
        user.setAge(16);
        user.setPassword("12312312");
        String result = JSON.toJSONString(user);
        // 返回到页面
        resp.getWriter().write(result);
    }
}

package com.cmbt.bean;

import lombok.Data;

@Data
public class Permission {
    private Integer permissionId;// 权限id
    private String permissionName;// 权限名
    private String permissionTitle;// 权限别名
    private Integer permissionPid;// 上级id，如果为null，说明它是第一级
    private String remark;// 备注
}

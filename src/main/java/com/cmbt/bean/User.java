package com.cmbt.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Integer userId;
    private String userAcct;
    private String password;
    private String username;
    private Integer age;
    private String email;
    private String phone;
    private String gender;
    private String roleName;
}

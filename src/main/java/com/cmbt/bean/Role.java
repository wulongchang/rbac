package com.cmbt.bean;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@Data
public class Role {
    private Integer roleId;// 角色Id
    private String roleName;// 角色名
    private String remark;// 备注
    private String permissions;

}

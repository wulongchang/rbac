package com.cmbt.bean;

import lombok.Data;

@Data
public class Page {
    private int pageSize;
    private int totalPage;
    private int Index;
    private int currentPage;
}

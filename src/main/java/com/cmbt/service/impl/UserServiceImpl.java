package com.cmbt.service.impl;

import com.cmbt.bean.Page;
import com.cmbt.bean.User;
import com.cmbt.dao.api.UserDao;
import com.cmbt.dao.impl.UserDaoImpl;
import com.cmbt.service.api.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
    UserDao userDao = new UserDaoImpl();

    @Override
    public List<User> getAllUserList() {
        List<User> users = userDao.selectAllUserList();
        return users;
    }

    /**
     * 如果账号密码在数据库中都存在且一致返回User对象，否则返回null
     *
     * @param userAcct
     * @param password
     * @return
     */
    @Override
    public User login(String userAcct, String password) {
        // 根据账户名查找该用户
        User  user = userDao.selectUserByUserAcct(userAcct);

        // 如果密码也一致，说明有此用户
        if(user != null && password.equals(user.getPassword())){
            return user;
        }else {
            // 如果不存在或不一致则返回null
            return null;
        }
    }

    @Override
    public void removeUsers(ArrayList<Integer> userIds) {

        for (int i = 0; i < userIds.size(); i++) {
            // 根据用户id删除对应的角色关系
            userDao.deleteUserAndRoleByUserId(userIds.get(i));
            // 根据id删除用户
            userDao.deleteUserById(userIds.get(i));
        }

    }

    /**
     * 如果数据库中已经存在该账户名，那么不允许注册，返回false
     * 如果数据库没有此账户名，则成功注册，返回true
     * @param user
     * @return
     */
    @Override
    public boolean register(User user) {
        // 查询是否存在该账户名
        User existUser = userDao.selectUserByUserAcct(user.getUserAcct());
        // 返回的是null则说明不存在该用户
        if(existUser == null){
            // 插入新增用户
            userDao.insertUser(user);
            return true;
        }
        return false;
    }

    /**
     * 传入一个User对象
     * @param user
     * @return
     */
    @Override
    public User insertUser(User user){
          return userDao.insertUser(user);
    }


    /**
     * 传入一个User对象,一个角色的id数组
     * @param user,roleList
     * @return
     */
    @Override
    public void insertUserRole(User user, int[] roleList) {
        userDao.insertUserRole(user,roleList);
    }

    /**
     * 传入一个User对象,一个角色的id数组
     * @param user,roleList
     * @return
     */
    @Override
    public void deleteUserRole(User user, int[] roleList) {
        userDao.deleteUserRole(user,roleList);
    }

    /**
     * 传入一个User对象
     * @param user
     * @return
     */
    @Override
    public User updateUser(User user){
        return userDao.updateUser(user);
    }

    @Override
    public List<User> queryUserByPage(Page pg,String keyword) {
        List<User> userList = userDao.selectUserListByPage(pg,keyword);
        return userList;
    }

    @Override
    public int getTotalPage(int pageSize, String keyword) {
        int totalPage = userDao.selectTotalPage(pageSize,keyword);
        return totalPage;
    }
}

package com.cmbt.service.impl;

import com.cmbt.bean.Permission;
import com.cmbt.dao.api.PermissionDao;

import com.cmbt.dao.impl.PermissionDaoImpl;

import com.cmbt.service.api.PermissionService;

import java.util.List;

public class PermissionServiceImpl implements PermissionService {
    PermissionDao permissionDao = new PermissionDaoImpl();
    @Override
    public List<Permission> getPermissionList() {
        List<Permission> permissionsList = permissionDao.selectPermissionList();
        return  permissionsList;

    }

}

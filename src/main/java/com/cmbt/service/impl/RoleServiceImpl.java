package com.cmbt.service.impl;

import com.cmbt.bean.Page;
import com.cmbt.bean.Role;
import com.cmbt.dao.api.RoleDao;
import com.cmbt.dao.impl.RoleDaoImpl;
import com.cmbt.service.api.RoleService;

import java.util.ArrayList;
import java.util.List;

public class RoleServiceImpl implements RoleService {
    RoleDao roleDao = new RoleDaoImpl();

    @Override
    public List<Role> selectAllRoleList() {
        return roleDao.selectAllRoleList();
    }

    @Override
    public List<Role> queryUserByPage(Page pg,String keyword) {
        List<Role> roleList = roleDao.selectRoleListByPage(pg,keyword);
        return roleList;
    }

    @Override
    public int getTotalPage(int pageSize,String keyword){
        int totalPage = roleDao.selectTotalPage(pageSize,keyword);
        return totalPage;
    }


    @Override
    public List<Role> getRoles() {
        List<Role> roleList = roleDao.selectAllRoleList();
        return roleList;
    }

    @Override
    public boolean addRole(Role role, String[] permissionIds) {
        boolean b = true;
        //1.保存角色信息，获取生成的角色的ID
        int roleId  = roleDao.addRole(role);
        //2.保存角色和菜单的关联
        for (int i = 0; i < permissionIds.length; i++) {
            int permissionId = Integer.parseInt(permissionIds[i]);
            int j = roleDao.addRoleAndPermission(roleId, permissionId);
            b = b&& j>0;
        }
        return b;
    }


    @Override
    public Role getRoleById(int roleId) {
        return roleDao.selectRoleById(roleId);
    }

    @Override
    public List<Integer> getPermissionIdsByRoleId(int roleId) {
        return roleDao.selectPermissionIdsByRoleId(roleId);
    }

    @Override
    public boolean updateRole(Role role, String[] permissionIds) {
        //1.修改角色信息
        int i = roleDao.modifyRole(role);
        //2.删除当前角色的原始权限
        roleDao.deleteRoleAndPermission(role.getRoleId());
        //3.新增所有新选的权限
        for(int k = 0;k < permissionIds.length;k++){
            int permissionId =Integer.parseInt(permissionIds[k]);
            int m = roleDao.addRoleAndPermission(role.getRoleId(), permissionId);
        }
        //对于修改角色，角色是可以没有权限的，只要i>0就表示角色修改成功
        return i>0;

    }

    @Override
    public List<Integer> selectRoleIdByUserId(int userId) {
        return roleDao.selectRoleIdByUserId(userId);
    }
    @Override
    public void removeRoles(ArrayList<Integer> roleIds) {
        for (int i = 0; i < roleIds.size(); i++) {
            // 根据角色id删除对应的权限关系
            roleDao.deleteRoleAndPermission(roleIds.get(i));
            // 根据角色id删除对应的用户与角色的关系
            roleDao.deleteUserAndRoleByRoleId(roleIds.get(i));
            // 根据id删除角色
            roleDao.deleteRoleById(roleIds.get(i));
        }
    }
}

package com.cmbt.service.api;

import com.cmbt.bean.Page;
import com.cmbt.bean.Role;

import java.util.ArrayList;
import java.util.List;

public interface RoleService {
    int getTotalPage(int pageSize,String keyword);

    List<Role> queryUserByPage(Page page,String kyword);

    List<Role> selectAllRoleList();


    List<Role> getRoles();


    boolean addRole(Role role,String[] permissionIds);

    Role getRoleById(int roleId);

    List<Integer> getPermissionIdsByRoleId(int roleId);

    boolean updateRole(Role role,String[] permissionId );

    List<Integer> selectRoleIdByUserId(int userId);

    void removeRoles(ArrayList<Integer> roleIds);
}

package com.cmbt.service.api;

import com.cmbt.bean.Page;
import com.cmbt.bean.User;

import java.util.ArrayList;
import java.util.List;

public interface UserService {
    List<User> getAllUserList();

    User login(String userAcct, String password);


    boolean register(User user);
//分页展示和查询
    List<User> queryUserByPage(Page pg,String keyword);


    int getTotalPage(int pageSize,String keyword);


    User insertUser(User user);

    void insertUserRole(User user,int[] roleList);

    void deleteUserRole(User user ,int[] roleList);

    User updateUser(User user);




    void removeUsers(ArrayList<Integer> userIds);
}

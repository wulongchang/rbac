package com.cmbt.service.api;

import com.cmbt.bean.Permission;

import java.util.List;

public interface PermissionService {
    List<Permission> getPermissionList();
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="GB18030">
<head>
    <meta charset="GB18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%--base标签--%>
    <base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        .tree li {
            list-style-type: none;
            cursor: pointer;
        }

        table tbody tr:nth-child(odd) {
            background: #F4F4F4;
        }

        table tbody td:nth-child(even) {
            color: #C00;
        }
    </style>

</head>

<body>
<%@include file="common-fluid.jsp" %>
<div class="container-fluid">
    <div class="row">
        <%@ include file="common-sidebar.jsp" %>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> 数据列表</h3>
			  </div>
			  <div class="panel-body">
<form class="form-inline"  action="roleController" role="form" style="float:left;">
    <input type="hidden" name="function" value="rolePageServlet"/>
  <div class="form-group has-feedback">
    <div class="input-group">
      <div class="input-group-addon">查询条件</div>
      <input class="form-control has-success" name="keyword" value="${keyword}" type="text" placeholder="请输入查询条件">
    </div>
  </div>
    <button type="submit"  class="btn btn-warning"><i class="glyphicon glyphicon-search"></i> 查询
    </button>
</form>
                  <button type="button" id="delSelected" class="btn btn-danger" style="float:right;margin-left:10px;"><i
                          class=" glyphicon glyphicon-remove"></i> 删除
                  </button>
<button type="button" class="btn btn-primary" style="float:right;"
        onclick="window.location.href='addrole.jsp'"><i class="glyphicon glyphicon-plus"></i> 新增</button>
<br>
 <hr style="clear:both;">
          <div class="table-responsive">
            <table class="table  table-bordered">
              <thead>
                <tr >
                  <th width="60">角色id</th>
				  <th width="30"><input type="checkbox"></th>
                  <th>名称</th>
                  <th width="100">操作</th>
                </tr>
              </thead>
              <tbody>
              <c:forEach  items="${roleList}" var="role">
                  <tr>
                      <td>${role.roleId}</td>
                      <td><input type="checkbox" value="${role.roleId}"></td>
                      <td>${role.roleName}</td>

                      <td hidden="hidden">${role.remark}</td>
                      <td hidden="hidden">${role.permissions}</td>
                      <td>
                          <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                  data-target="#roleModal"><i class=" glyphicon glyphicon-check"></i>
                          </button>

                          <!-- 模态框（Modal） -->
                          <div class="modal fade" id="roleModal" tabindex="-1" role="dialog"
                               aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal"
                                                  aria-hidden="true">&times;
                                          </button>
                                          <h4 class="modal-title" id="myModalLabel">角色条目详情</h4>
                                      </div>
                                      <div class="modal-body">
                                          <table class="table table-striped">
                                              <tr>
                                                  <td>角色条目编号</td>
                                                  <td><input type="text" id="rId" name="rId"
                                                             class="form-control" readonly/></td>
                                              </tr>
                                              <tr>
                                                  <td>角色名称</td>
                                                  <td><input type="text" id="rName" name="rName"
                                                             class="form-control" readonly/></td>
                                              </tr>
                                              <tr>
                                                  <td>信息备注</td>
                                                  <td><input type="text" id="remark" name="remark"
                                                             class="form-control" readonly/></td>
                                              </tr>
                                              <tr>
                                                  <td>角色权限</td>
                                                  <td><input type="text" id="permissions" name="permissions"class="form-control" readonly/></td>
                                              </tr>
                                          </table>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default"
                                                  data-dismiss="modal">关闭
                                          </button>
                                      </div>
                                  </div><!-- /.modal-content -->
                              </div><!-- /.modal -->
                          </div><!-- /.modal -->
                          <button type="button" class="btn btn-primary btn-xs"><i
                                  class=" glyphicon glyphicon-pencil" onclick="window.location.href = 'modifyrole.jsp?roleId=${role.roleId}&roleName=${role.roleName}&remark=${role.remark}'"></i></button>

                          <button type="button" name = "remove" class="btn btn-danger btn-xs"><i
                                  class=" glyphicon glyphicon-remove"></i></button>
                      </td>
                  </tr>
              </c:forEach>


                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6" align="center">
                                    <ul class="pagination">
                                        <li id="selector"><a
                                                href="roleController?function=rolePageServlet&currentPage=1&keyword=${requestScope.keyword}">首页</a>
                                        </li>
                                        <li>
                                            <a href="roleController?function=rolePageServlet&currentPage=${requestScope.prePage}&keyword=${requestScope.keyword}">上一页<span
                                                    class="sr-only">(current)</span></a>
                                        </li>

                                        <li>
                                            <a href="roleController?function=rolePageServlet&currentPage=${requestScope.nextPage}&keyword=${requestScope.keyword}">下一页</a>
                                        </li>
                                        <li>
                                            <a href="roleController?function=rolePageServlet&currentPage=${requestScope.totalPage}&keyword=${requestScope.keyword}">尾页</a>
                                        </li>
                                    </ul>
                                    <p>第 <a href="">${requestScope.currentPage}</a> 页</p>
                                </td>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<%--引入layer--%>
<script src="layer/layer.js"></script>
<script type="text/javascript">
    $(function () {
        var isFirst = true;
        <c:if test="${!empty isFirst}">
        isFirst = false;
        </c:if>
        if (isFirst == true) {
            location.href = "roleController?function=rolePageServlet&currentPage=1"
        }

        // 单击删除
        $("button[name=remove]").click(function () {
            // 获取删除的用户的id
            var roleId = $(this).parent().parent().children().eq(0).text();
            var roleIds = [];
            roleIds.push(roleId)
            // 删除操作
            deleteRoles(roleIds);
        });


        // 多选删除
        $("#delSelected").click(function (){
            var roleIds = [];
            // 获取选中的角色Id集合
            $("input[type=checkbox]:checked").each(function () {
                roleIds.push($(this).val())
            });
            // 删除操作
            deleteRoles(roleIds);
        });

        // 根据数组删除角色
        function deleteRoles(roleIds){
            // 发送Ajax请求进行删除操作
            if (confirm("你确定要删除吗?")) {
                $.ajax({
                        "url": "roleController",
                        "type": "post",
                        "data": {
                            "function": "removeRoles",// 指定哪个方法来处理
                            "roleIds": roleIds,
                        },
                        "dataType": "json",
                        "success": function (response) {
                            console.log(response);
                            var result = response.result;
                            if (result == "FAILED") {
                                layer.msg(response.message)
                            } else if (result == "SUCCESS") {
                                layer.msg("删除成功")
                                // 回到删除的那个页面，重新查一次数据库
                                location.href="roleController?function=rolePageServlet&currentPage=${currentPage}&keyword=${requestScope.keyword}";
                            }
                        },
                        "error": function (response) {
                            // 显示错误状态码和文本信息
                            layer.msg(response.status + " " + response.statusText)
                        }
                    }
                );
            }
        }


        $(".list-group-item").click(function () {
            if ($(this).find("ul")) {
                $(this).toggleClass("tree-closed");
                if ($(this).hasClass("tree-closed")) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });

    // $("tbody .btn-success").click(function(){
    //     window.location.href = "assignPermission.html";
    // });

    $('#roleModal').on('show.bs.modal', function (event) {
        $("textarea[name = refuseTextareaContext]").innerHTML = "";
        var btnThis = $(event.relatedTarget); //触发事件的按钮
        var modal = $(this);  //当前模态框
        var modalId = btnThis.data('id');   //解析出data-id的内容
        currentDataRoleId = btnThis.closest('tr').find('td').eq(0).text(); //获取a标签所在行的某一列的内容,eq括号里面的数值表示列数
        $("#rId").val(currentDataRoleId); // 设置模态框里id为cId的组件的值为currentDataId
        currentDataRoleName = btnThis.closest('tr').find('td').eq(2).text();
        $("#rName").val(currentDataRoleName);
        currentDataRemark = btnThis.closest('tr').find('td').eq(3).text();
        $("#remark").val(currentDataRemark);
        currentDataPermissions = btnThis.closest('tr').find('td').eq(4).text();
        $("#permissions").val(currentDataPermissions);
    });
</script>
</body>
</html>

<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Loding font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
    <!-- Custom Styles -->
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <%--引入jquery--%>
    <script src="jquery/jquery-2.1.1.min.js"></script>
    <%--引入layer--%>
    <script src="layer/layer.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <title>Home</title>
</head>

<script type="text/javascript">
    // 等待页面加载完再执行
    $(function () {
        /*给登录按钮绑定单击函数*/
        $("#submitBtn").click(function () {
            // 获取账户名
            var userAcct = $("#loginUserAcct").val();
            // 获取密码
            var password = $("#loginPassword").val();

            // 发送ajax请求
            $.ajax({
                "url": "userController",
                "type": "post",
                "data": {
                    "function": "login",// 指定哪个方法来处理
                    "userAcct": userAcct,
                    "password": password,
                },
                "dataType": "json",
                "success": function (response) {
                    var result = response.result;
                    if (result == "FAILED") {
                        layer.msg(response.message)
                    } else if (result == "SUCCESS") {
                        // 跳转到主页
                        location.href = "main.jsp";
                    }
                },
                "error": function (response) {
                    // 显示错误状态码和文本信息
                    layer.msg(response.status + " " + response.statusText)
                }
            });
            return false;
        });

        // 给"没有账号"按钮绑定单击事件，点击时打开模态框
        $("#a-modal-register").click(function () {
            $("#modal-register").modal("show");
            return false;
        });

        // 给"注册按钮"绑定单击事件
        $("#registerBth").click(function (){
            // 获取账户名
            var userAcct = $("#registerUserAcct").val();
            // 获取密码
            var password = $("#registerPassword").val();
            // 获取用户姓名
            var username = $("#registerUsername").val();
            // 获取年龄
            var age = $("#registerAge").val();
            // 获取电话
            var phone = $("#registerPhone").val();
            // 获取邮箱
            var email = $("#registerEmail").val();
            // 获取性别
            var gender = $("input[name=gender]").val();

            // 发送ajax请求
            $.ajax({
                "url": "userController",
                "type": "post",
                "data": {
                    "function": "register",// 指定哪个方法来处理
                    "userAcct": userAcct,
                    "password": password,
                    "username":username,
                    "age":age,
                    "phone":phone,
                    "gender":gender,
                    "email":email
                },
                "dataType": "json",
                "success": function (response) {
                    var result = response.result;
                    if (result == "FAILED") {
                        layer.msg(response.message)
                    } else if (result == "SUCCESS") {
                        layer.msg("注册成功！")
                        // 关闭模态框
                        $("#modal-register").modal("hide");
                    }
                },
                "error": function (response) {
                    // 显示错误状态码和文本信息
                    layer.msg(response.status + " " + response.statusText)
                }
            });

            // 禁止默认行为
            return false;
        });
    });
</script>
<body>

<!-- Backgrounds -->

<div id="login-bg" class="container-fluid">

    <div class="bg-img"></div>
    <div class="bg-color"></div>
</div>

<!-- End Backgrounds -->

<div class="container" id="login">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="login">

                <h1>登录</h1>

                <!-- Loging form -->
                <form action="" method="post">
                    <div class="form-group">
                        <input type="text" id="loginUserAcct" name="userAcct" class="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="用户名">

                    </div>
                    <div class="form-group">
                        <input type="password" id="loginPassword" name="password" class="form-control" id="exampleInputPassword1"
                               placeholder="密码">
                    </div>

                    <div class="form-check">

                        <%--                        <label class="switch">--%>
                        <%--                            <input type="checkbox">--%>
                        <%--&lt;%&ndash;                            <span class="slider round"></span>&ndash;%&gt;--%>
                        <%--                        </label>--%>
                        <%--                        <label class="form-check-label" for="exampleCheck1">Remember me</label>--%>

                        <label class="forgot-password"><a id="a-modal-register" href="#">没有账号?</a></label>


                    </div>
                    <br>
                    <button type="submit" id="submitBtn" class="btn btn-lg btn-block btn-success">登录</button>
                </form>
                <!-- End Loging form -->

            </div>
        </div>

    </div>
</div>
<!-- MODAL -->
<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="modal-register-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title" id="modal-register-label">注册</h3>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
            </div>

            <div class="modal-body">

                <form role="form" action="" method="post" class="registration-form">
                    <div class="form-group">
                        <label class="sr-only" for="registerUserAcct">userAcct</label>
                        <input type="text" id="registerUserAcct" name="userAcct" placeholder="账户名..."
                               class="form-first-name form-control" >
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="registerPassword">password</label>
                        <input type="text" name="password" placeholder="密码..."
                               class="form-last-name form-control" id="registerPassword">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="registerUsername">username</label>
                        <input type="text" name="username" placeholder="用户姓名..." class="form-first-name form-control"
                               id="registerUsername">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="registerPhone">phone</label>
                        <input type="text" name="phone" placeholder="电话..." class="form-first-name form-control"
                               id="registerPhone">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="registerEmail">Email</label>
                        <input type="text" name="email" placeholder="Email..." class="form-email form-control"
                               id="registerEmail">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="registerAge">age</label>
                        <input type="text" name="age" placeholder="年龄..." class="form-first-name form-control"
                               id="registerAge">
                    </div>
                    <!-- 水平 -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="radio-inline">
                                <label for="women"><input type="radio" name="gender" id="women" value="男">男</label>
                            </div>
                            <div class="radio-inline">
                                <label for="man"><input type="radio" name="gender" id="man" value="女">女</label>
                            </div>
                        </div>
                    </div>
<%--                    <div class="form-group">--%>
<%--                        <label class="sr-only" for="form-about-yourself">About yourself</label>--%>
<%--                        <textarea name="form-about-yourself" placeholder="About yourself..."--%>
<%--                                  class="form-about-yourself form-control" id="form-about-yourself"></textarea>--%>
<%--                    </div>--%>
                    <div style="text-align:center">
                        <button type="submit" id="registerBth"  class="btn btn-success btn-lg align-content-lg-center">注册</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
</body>

</html>
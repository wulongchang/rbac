<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="GB18030">
<head>
    <meta charset="GB18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        .tree li {
            list-style-type: none;
            cursor: pointer;
        }

        table tbody tr:nth-child(odd) {
            background: #F4F4F4;
        }

        table tbody td:nth-child(even) {
            color: #C00;
        }
    </style>

    <script type="text/javascript" src="jquery/jquery-2.1.1.min.js"></script>
    <%--引入layer--%>
    <script src="layer/layer.js"></script>

    <script type="text/javascript">
        $(function () {
            var isFirst = true;
            <c:if test="${!empty isFirst}">
            isFirst = false;
            </c:if>
            if (isFirst==true){
                location.href="userController?function=userPageServlet&currentPage=1"
            }

            // 单击删除
            $("button[name=remove]").click(function () {
                // 获取删除的用户的id
                var userId = $(this).parent().parent().children().eq(0).text();
                var userIds = [];
                userIds.push(userId)
                deleteUsers(userIds);
            });

            // 多选删除
            $("#delSelected").click(function (){
                    var userIds = [];
                    // 获取选中的角色Id集合
                    $("input[type=checkbox]:checked").each(function () {
                        userIds.push($(this).val())
                    });
                    // 删除操作
                    deleteUsers(userIds);
            });

            function deleteUsers(userIds){
                // 发送Ajax请求进行删除操作
                if (confirm("你确定要删除吗?")) {
                    $.ajax({
                            "url": "userController",
                            "type": "post",
                            "data": {
                                "function": "removeUsers",// 指定哪个方法来处理
                                "userIds": userIds,
                            },
                            "dataType": "json",
                            "success": function (response) {
                                console.log(response);
                                var result = response.result;
                                if (result == "FAILED") {
                                    layer.msg(response.message)
                                } else if (result == "SUCCESS") {
                                    layer.msg("删除成功")
                                    // 回到删除的那个页面，重新查一次数据库
                                    location.href="userController?function=userPageServlet&currentPage=${currentPage}&keyword=${requestScope.keyword}";
                                }
                            },
                            "error": function (response) {
                                // 显示错误状态码和文本信息
                                layer.msg(response.status + " " + response.statusText)
                            }
                        }
                    );
                }
            }
        });

    </script>
</head>

<body>

<%@include file="common-fluid.jsp" %>

<div class="container-fluid">
    <div class="row">
        <%@ include file="common-sidebar.jsp" %>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> 数据列表</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" action="userController" method="get" role="form" style="float:left;">
                        <input type="hidden" name="function" value="userPageServlet"/>
<%--                        相当于userController?function=userPageServlet--%>
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon">查询条件</div>
                                <input  name="keyword" class="form-control has-success" type="text" placeholder="请输入查询条件" value="${keyword}">
                            </div>
                        </div>
                        <button id="pageQuery" type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-search"></i> 查询
                        </button>
                    </form>
                    <button type="button" id="delSelected" class="btn btn-danger" style="float:right;margin-left:10px;">
                        <i class=" glyphicon glyphicon-remove"></i> 删除
                    </button>
                    <button type="button" class="btn btn-primary" style="float:right;"
                            onclick="window.location.href='add.jsp'"><i class="glyphicon glyphicon-plus"></i> 新增
                    </button>
                    <br>
                    <hr style="clear:both;">
                    <div class="table-responsive">

                        <form id="form1" action="upload" method="get">
                            <table class="table  table-bordered">
                                <thead>
                                <tr>
                                    <th width="30">#</th>
                                    <th width="30"><input type="checkbox" id="firstCb"></th>
                                    <th>账号</th>
                                    <th>名称</th>
                                    <th>邮箱地址</th>
                                    <th width="100">操作</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">


                                <c:forEach var="user" items="${userList}">
                                    <tr>
                                        <td>${user.userId}</td>
                                        <td><input type="checkbox" value="${user.userId}"></td>
                                        <td >${user.userAcct}</td>
                                        <td>${user.username}</td>
                                        <td>${user.email}</td>
                                        <td hidden="hidden">${user.age}</td>
                                        <td hidden="hidden">${user.phone}</td>
                                        <td hidden="hidden">${user.gender}</td>
                                        <td hidden="hidden">${user.roleName}</td>
                                        <td>
                                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#userModal">
                                                <i class=" glyphicon glyphicon-check"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary btn-xs"><i
                                                    class=" glyphicon glyphicon-pencil" onclick="window.location.href ='edit.jsp?userId=${user.userId}&userAcct=${user.userAcct}&password=${user.password}&username=${user.username}&age=${user.age}&email=${user.email}&phone=${user.phone}&gender=${user.gender}'"></i></button>

                                            <!-- 模态框（Modal） -->
                                            <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel">用户详情</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td>用户编号</td>
                                                                    <td><input type="text" id="uId" name="uId" class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>用户名</td>
                                                                    <td><input type="text" id="account" name="account" class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>名称</td>
                                                                    <td><input type="text" id="name" name="name"class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>角色</td>
                                                                    <td><input type="text" id="roleName" name="roleName"class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>电子邮箱</td>
                                                                    <td><input type="text" id="email" name="email"class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>年龄</td>
                                                                    <td><input type="text" id="age" name="age"class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>电话</td>
                                                                    <td><input type="text" id="phone" name="phone"class="form-control" readonly/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>性别</td>
                                                                    <td><input type="text" id="gender" name="gender"class="form-control" readonly/></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal -->
                                            </div><!-- /.modal -->


                                            <button type="button" name="remove" class="btn btn-danger btn-xs"><i
                                                    class=" glyphicon glyphicon-remove"></i></button>
                                        </td>
                                    </tr>
                                </c:forEach>

                                </tbody>


                                <tfoot>
                                <tr>
                                    <td colspan="6" align="center">

                                        <ul class="pagination">
                                            <%--<li class="disabled">--%>
                                            <li ><a href="userController?function=userPageServlet&currentPage=1&keyword=${requestScope.keyword}"> 首页</a>
                                            </li>
                                            <li>
                                                <a href="userController?function=userPageServlet&currentPage=${requestScope.prePage }&keyword=${requestScope.keyword}">上一页<span
                                                        class="sr-only">(current)</span></a>
                                            </li>

                                            <li>
                                                <a href="userController?function=userPageServlet&currentPage=${requestScope.nextPage}&keyword=${requestScope.keyword}">下一页</a>
                                            </li>
                                            <li>
                                                <a href="userController?function=userPageServlet&currentPage=${requestScope.totalPage}&keyword=${requestScope.keyword}">尾页</a>
                                            </li>
                                        </ul>

                                        <p>第 <a href="">${requestScope.currentPage}</a>  页</p>

                                    </td>
                                </tr>

                                </tfoot>
                            </table>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<script type="text/javascript">
    $(function () {
        $(".list-group-item").click(function () {
            if ($(this).find("ul")) {
                $(this).toggleClass("tree-closed");
                if ($(this).hasClass("tree-closed")) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });

    // $("tbody .btn-success").click(function () {
    //     window.location.href = "assignRole.html";
    // });

    $('#userModal').on('show.bs.modal', function (event) {
        $("textarea[name = refuseTextareaContext]").innerHTML = "";
        var btnThis = $(event.relatedTarget); //触发事件的按钮
        var modal = $(this);  //当前模态框
        var modalId = btnThis.data('id');   //解析出data-id的内容
        currentDataId = btnThis.closest('tr').find('td').eq(0).text(); //获取a标签所在行的某一列的内容,eq括号里面的数值表示列数
        $("#uId").val(currentDataId); // 设置模态框里id为cId的组件的值为currentDataId
        currentDataAcct = btnThis.closest('tr').find('td').eq(2).text();
        $("#account").val(currentDataAcct);
        currentDataName = btnThis.closest('tr').find('td').eq(3).text();
        $("#name").val(currentDataName);
        currentDataEmail = btnThis.closest('tr').find('td').eq(4).text();
        $("#email").val(currentDataEmail);
        currentDataAge = btnThis.closest('tr').find('td').eq(5).text();
        $("#age").val(currentDataAge);
        currentDataPhone = btnThis.closest('tr').find('td').eq(6).text();
        $("#phone").val(currentDataPhone);
        currentDataGender = btnThis.closest('tr').find('td').eq(7).text();
        $("#gender").val(currentDataGender);
        currentDataRole = btnThis.closest('tr').find('td').eq(8).text();
        $("#roleName").val(currentDataRole);
    });

  /*  $("tbody .btn-primary").click(function () {
        window.location.href = "edit.jsp?userAcct=${user.userAcct}";
    });*/
</script>
</body>
</html>

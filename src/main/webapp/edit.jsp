<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: long
  Date: 2022/12/22
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="GB18030">
<head>
	<meta charset="GB18030">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/doc.min.css">
	<style>
		.tree li {
			list-style-type: none;
			cursor:pointer;
		}
	</style>
</head>

<body>

<%@include file="common-fluid.jsp"%>

<div class="container-fluid">
	<div class="row">
		<%@include file="common-sidebar.jsp"%>
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<ol class="breadcrumb">
				<li><a href="main.jsp">首页</a></li>
				<li><a href="user.jsp">数据列表</a></li>
				<li class="active">修改</li>
			</ol>
			<div class="panel panel-default">
				<div class="panel-heading">表单数据<div style="float:right;cursor:pointer;" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-question-sign"></i></div></div>
				<div class="panel-body">
					<form role="form" action="userController?function=updateUser&userId=<%=request.getParameter("userId")%>" method="post">
						<div class="form-group">
							<label for="exampleInputPassword1">登陆账号</label>
							<input type="text" name="userAcct" class="form-control"  value="<%=request.getParameter("userAcct")%>" placeholder="<%=request.getParameter("userAcct")%>" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">登陆密码</label>
							<input type="text" name="password" class="form-control"  value="<%=request.getParameter("password")%>">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">用户名称</label>
							<input type="text" name="userName" class="form-control"  value="<%=request.getParameter("username")%>">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">用户年龄</label>
							<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')"   onblur="value=value.replace(/[^\d]/g,'')" name="userAge" class="form-control"  value="<%=request.getParameter("age")%>">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">邮箱地址</label>
							<input type="email" name="userEmail" class="form-control" id="exampleInputEmail1" value="<%=request.getParameter("email")%>">
							<p class="help-block label label-warning">请输入合法的邮箱地址, 格式为： xxxx@xxxx.com</p>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">手机号码</label>
							<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')"   onblur="value=value.replace(/[^\d]/g,'')" name="phone" class="form-control" id="exampleInputPassword1" value="<%=request.getParameter("phone")%>">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">性别</label>
							<input type="radio" name="sex" value="男" >男
							<input type="radio" name="sex" value="女" >女
						</div>
						<div id="addcheckbox" class="form-group">
							<label>角色选择
								<tr>

									<td>

									</td>
								</tr>
							</label>
						</div>
						<button type="submit" class="btn btn-success" onclick="return check(this.form)"><i class="glyphicon glyphicon-edit"></i>修改</button>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">帮助</h4>
			</div>
			<div class="modal-body">
				<div class="bs-callout bs-callout-info">
					<h4>测试标题1</h4>
					<p>测试内容1，测试内容1，测试内容1，测试内容1，测试内容1，测试内容1</p>
				</div>
				<div class="bs-callout bs-callout-info">
					<h4>测试标题2</h4>
					<p>测试内容2，测试内容2，测试内容2，测试内容2，测试内容2，测试内容2</p>
				</div>
			</div>
			<!--
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            -->
		</div>
	</div>
</div>
<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<script src="layer/layer.js"></script>
<script type="text/javascript">
	$(function () {
		// 查询所有角色，显示
		$.ajax({
			"url":"roleController",
			"type":"post",
			"data": {
				"function":"getRolesEdit",
			},
			dataType:"json",
			"success": function (response) {
				var result = response.result;
				var data = response.data;
				console.log(data)
				for ( var i = 0; i < data.length; i++) {
					$("#addcheckbox").append("<label><input type='checkbox' name='roleName' value='"+data[i].roleId+"'>"+data[i].roleName+"</label>");
				}
			},
			"error": function (response) {
				// 显示错误状态码和文本信息
				layer.msg(response.status + " " + response.statusText)
			}
		})
		// 查询用户对应的角色，做一个选中状态
		// 获取用户id
		var userId = <%=request.getParameter("userId")%>
		$.ajax({
			"url":"roleController",
			"type":"post",
			"data": {
				"function":"getUserRoles",
				"userId":userId
			},
			dataType:"json",
			"success": function (response) {
				var data=response.data;
				console.log(data)
				// // 把对应权限做一个回显
				//  var input=$("input [name= permissions]")
				//  console.log(input)

				// $("input [name= permissions]")
				var checkBoxAll = $("input[name='roleName']");
				for(var i=0;i<data.length;i++){
					//获取所有复选框对象的value属性，然后，用data[i]和他们匹配，如果有，则说明他应被选中
					$.each(checkBoxAll,function(j,checkbox){
						//获取复选框的value属性
						var checkValue=$(checkbox).val();
						if(data[i]==checkValue){
							$(checkbox).attr("checked",true);
						}
					})
				}

			},
			"error": function (response) {
				// 显示错误状态码和文本信息
				layer.msg(response.status + " " + response.statusText)
			}

		})
	/*	// 如果用户点击了编辑的提交，发送编辑请求
		var checkID = [];//定义一个空数组
		$("input[name='roleName']:checked").each(function (i) {//把所有被选中的复选框的值存入数组
			checkID[i] = $(this).val();
		});*/





		$(".list-group-item").click(function(){
			if ( $(this).find("ul") ) {
				$(this).toggleClass("tree-closed");
				if ( $(this).hasClass("tree-closed") ) {
					$("ul", this).hide("fast");
				} else {
					$("ul", this).show("fast");
				}
			}
		});
	});
</script>
<script type="text/javascript">
	window.onload=function (){
		let radio= document.getElementsByName("sex");
		<%String gender = request.getParameter("gender");%>
		<%if(gender.equals("男")){%>radio[0].checked=true;
		radio[1].checked=false;<%} else {%>radio[0].checked=false;
		radio[1].checked=true;<%}%>

	}
	function check(form){

		if (form.password.value==""){
			alert("请输入登录密码!");
			form.password.focus();
			return false;
		}
		if (form.userName.value==""){
			alert("请输入用户姓名");
			form.userName.focus();
			return false;
		}
		if (form.userAge.value==""){
			alert("请输入用户年龄");
			form.userAge.focus();
			return false;
		}
		if (form.userEmail.value==""){
			alert("请输入用户年龄");
			form.userEmail.focus();
			return false;
		}
		var email = form.userEmail.value;
		if(email != "") {
			var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
			//调用正则验证test()函数
			isok= reg.test(email);
			if(!isok) {
				alert("邮箱格式不正确，请重新输入！");
				form.userEmail.focus();
				return false;
			}
		};
		if (form.phone.value==""){
			alert("请输入用户年龄");
			form.phone.focus();
			return false;
		}
		layer.msg("修改成功");
		return true;
	}
</script>
</body>
</html>


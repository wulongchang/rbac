<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: long
  Date: 2022/12/22
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="GB18030">
<head>
    <meta charset="GB18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/doc.min.css">
    <style>
        .tree li {
            list-style-type: none;
            cursor:pointer;
        }
    </style>
</head>

<body>

<%@include file="common-fluid.jsp"%>

<div class="container-fluid">
    <div class="row">
        <%@include file="common-sidebar.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <ol class="breadcrumb">
                <li><a href="main.jsp">首页</a></li>
                <li><a href="user.jsp">数据列表</a></li>
                <li class="active">新增</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">表单数据<div style="float:right;cursor:pointer;" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-question-sign"></i></div></div>
                <div class="panel-body">
                    <form role="form" action="userController?function=insertUser" method="post">
                        <div class="form-group">
                            <label for="exampleInputPassword1">登陆账号</label>
                            <input type="text" name="userAcct" class="form-control"  placeholder="请输入登陆账号">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">登陆密码</label>
                            <input type="password" name="password" class="form-control"  placeholder="请输入登陆密码">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">用户名称</label>
                            <input type="text" name="userName" class="form-control"  placeholder="请输入用户名称">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">用户年龄</label>
                            <input type="text"  onkeyup="value=value.replace(/[^\d]/g,'')"   onblur="value=value.replace(/[^\d]/g,'')" name="userAge" class="form-control"  placeholder="请输入用户年龄">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">邮箱地址</label>
                            <input type="email" name="userEmail" class="form-control" id="exampleInputEmail1" placeholder="请输入邮箱地址">
                            <p class="help-block label label-warning">请输入合法的邮箱地址, 格式为： xxxx@xxxx.com</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">手机号码</label>
                            <input type="text" onkeyup="value=value.replace(/[^\d]/g,'')"   onblur="value=value.replace(/[^\d]/g,'')" name="phone" class="form-control" id="exampleInputPassword1" placeholder="请输入用户手机号码">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">性别</label>
                            <input type="radio" name="sex" value="男" checked>男
                            <input type="radio" name="sex" value="女">女
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">角色</label>
                            <c:forEach var="role" items="${roleList}">
                                <input type="checkbox" name="roleList" value="${role.roleId}">${role.roleName}
                            </c:forEach>
                        </div>
                        <button type="submit" class="btn btn-success" onclick="return check(this.form)"><i class="glyphicon glyphicon-plus"></i> 新增</button>
                        <button id="reset" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> 重置</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">帮助</h4>
            </div>
            <div class="modal-body">
                <div class="bs-callout bs-callout-info">
                    <h4>测试标题1</h4>
                    <p>测试内容1，测试内容1，测试内容1，测试内容1，测试内容1，测试内容1</p>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h4>测试标题2</h4>
                    <p>测试内容2，测试内容2，测试内容2，测试内容2，测试内容2，测试内容2</p>
                </div>
            </div>
            <!--
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            -->
        </div>
    </div>
</div>
<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<script src="layer/layer.js"></script>
<script type="text/javascript">
    $(function () {
        // 查询所有角色，显示
        var isFirst = true;
        <c:if test="${!empty isFirst}">
            isFirst = false;
        </c:if>
        if (isFirst==true) {
            location.href = "roleController?function=getRoles&jsp=add"
        }
        // 查询用户对应的角色，做一个选中状态
        // 如果用户点击了编辑的提交，发送编辑请求

        $(".list-group-item").click(function(){
            if ( $(this).find("ul") ) {
                $(this).toggleClass("tree-closed");
                if ( $(this).hasClass("tree-closed") ) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });
</script>
<script type="text/javascript">
    window.onload=function (){
        let reset = document.getElementById("reset");
        let inputs = document.getElementsByClassName("form-control");
        let  radio= document.getElementsByName("sex");
        reset.onclick=function (){
            for (i=0;i<inputs.length;i++){
                inputs[i].value=' ';
            }
            inputs[0].placeholder="请输入登陆账号";
            inputs[1].placeholder="请输入登陆密码";
            inputs[3].placeholder="请输入用户名称";
            inputs[4].placeholder="请输入用户年龄";
            inputs[6].placeholder="请输入用户手机号码";
            radio[0].checked=true;
            radio[1].checked=false;
        }

    }
    function check(form){
        if(form.userAcct.value==''){
            alert("请输入用户帐号!");
            form.userAcct.focus();
            return false;
        }
        if (form.password.value==""){
            alert("请输入登录密码!");
            form.password.focus();
            return false;
        }
        if (form.userName.value==""){
            alert("请输入用户姓名");
            form.userName.focus();
            return false;
        }
        if (form.userAge.value==""){
            alert("请输入用户年龄");
            form.userAge.focus();
            return false;
        }
        if (form.userEmail.value==""){
            alert("请输入用户邮箱");
            form.userEmail.focus();
            return false;
        }
        var email = form.userEmail.value;
        if(email != "") {
            var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
            //调用正则验证test()函数
            isok= reg.test(email);
            if(!isok) {
                alert("邮箱格式不正确，请重新输入！");
                form.userEmail.focus();
                return false;
            }
        };


        if (form.phone.value==""){
            alert("请输入用户手机号码");
            form.phone.focus();
            return false;
        }
        layer.msg("添加成功");
        return true;
    }
</script>
</body>
</html>


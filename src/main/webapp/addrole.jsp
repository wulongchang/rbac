
<%--
  Created by IntelliJ IDEA.
  User: long
  Date: 2022/12/22
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="GB18030">
<head>
    <meta charset="GB18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="${pageContext.request.scheme }://${pageContext.request.serverName }:${pageContext.request.serverPort }${pageContext.request.contextPath }/ "/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/doc.min.css">
    <script src="jquery/jquery-2.1.1.min.js"></script>
    <script src="layer/layer.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .tree li {
            list-style-type: none;
            cursor:pointer;
        }
    </style>
</head>

<script type="text/javascript">
    $(function () {


        /*给提交按钮绑定单击函数*/
        $("#submitBtn").click(function () {
            // 获取角色名
            var RoleName = $("#addRoleName").val();
            // 获取角色描述
            var Remark = $("#addRoleRemark").val();
            var checkID = [];//定义一个空数组
            $("input[name='permissions']:checked").each(function (i) {//把所有被选中的复选框的值存入数组
                checkID[i] = $(this).val();
            });

            // 发送ajax请求
            $.ajax({
                "url": "roleController",
                "type": "post",
                "data": {
                    "function": "addRole",// 指定哪个方法来处理
                    "roleName": RoleName,
                    "remark": Remark,
                    "permissionID[]": checkID
                },
                "dataType": "json",
                "success": function (response) {
                    var result = response.result;
                    if (result == "FAILED") {
                        layer.msg(response.message)
                    } else if (result == "SUCCESS") {
                        layer.msg("新增成功！", {
                            time: 1000,
                            end: function(){
                                window.location.href = "role.jsp";
                            }
                        });
                    }
                },
                "error": function (response) {
                    // 显示错误状态码和文本信息
                    layer.msg(response.status + " " + response.statusText)
                }
            });
            return false;
        });
    });
</script>





<body>

<%@include file="common-fluid.jsp"%>

<div class="container-fluid">
    <div class="row">
        <%@include file="common-sidebar.jsp"%>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <ol class="breadcrumb">
                <li><a href="#">首页</a></li>
                <li><a href="#">角色列表</a></li>
                <li class="active">新增</li>
            </ol>
            <div class="panel panel-default">
                <div class="panel-heading">表单数据<div style="float:right;cursor:pointer;" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-question-sign"></i></div></div>
                <div class="panel-body">
                    <form action="" method="post">

                        <div class="form-group">
                            <label >角色名:
                            <input type="text" id="addRoleName" name ="RoleName" placeholder="请输入角色名">
                            </label>
                        </div>
                        <div class="form-group">
                            <label >角色描述:
                            <input type="text" id="addRoleRemark" name ="Remark" placeholder="请输入角色描述">
                            </label>
                        </div>
                        <div class="form-group">
                            <label>权限选择
                            <tr>

                                <td>
<%--                                    <script type="text/javascript"><!--全选的方法 -->--%>
<%--                                    function selectAll() {--%>
<%--                                        var s=document.getElementsByName("like");--%>
<%--                                        for (var i= 0; i <s.length; i++) {--%>
<%--                                            s[i].checked=document.getElementsByName("all");--%>
<%--                                        }--%>
<%--                                    }--%>
<%--                                    </script>--%>
<%--                                    <input type="checkbox" name="permissions" value="全选" onclick="selectAll()">全选--%>
<%--                                    <input type="checkbox" name="permissions" value="1" id="1">用户--%>
<%--                                    <input type="checkbox" name="permissions" value="2" id="2">添加用户--%>
<%--                                    <input type="checkbox" name="permissions" value="3" id="3">编辑用户--%>
<%--                                    <input type="checkbox" name="permissions" value="4" id="4">删除用户--%>
<%--                                    <input type="checkbox" name="permissions" value="5" id="5">查询用户--%>
                                    <c:forEach items="${permissionList}" var = "permission">


                                                  <input type="checkbox" name="permissions" value="${permission.permissionId}">${permission.permissionTitle}


                                    </c:forEach>
                                </td>
                            </tr>
                            <tr >
                                </label>
                        </div>
                        <button type="submit" id="submitBtn" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> 提交</button>
                        <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> 重置</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">帮助</h4>
            </div>
            <div class="modal-body">
                <div class="bs-callout bs-callout-info">
                    <h4>测试标题1</h4>
                    <p>测试内容1，测试内容1，测试内容1，测试内容1，测试内容1，测试内容1</p>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h4>测试标题2</h4>
                    <p>测试内容2，测试内容2，测试内容2，测试内容2，测试内容2，测试内容2</p>
                </div>
            </div>
            <!--
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            -->
        </div>
    </div>
</div>
<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<script type="text/javascript">
    $(function () {
        var isFirst = true;
        <c:if test="${!empty isFirst}">
        isFirst = false;
        </c:if>
        if (isFirst==true){
            location.href="permissionController?function=perList2&toPage=addrole.jsp"
        }

        $(".list-group-item").click(function(){
            if ( $(this).find("ul") ) {
                $(this).toggleClass("tree-closed");
                if ( $(this).hasClass("tree-closed") ) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });
</script>
</body>
</html>


/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : rbac

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 06/01/2023 15:17:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission`  (
  `permission_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `permission_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限名称',
  `permission_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限的别名',
  `permission_pid` int(0) NULL DEFAULT NULL COMMENT '权限的上一级权限id，如果是null，说明了是顶级权限',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES (1, 'user', '用户', NULL, NULL);
INSERT INTO `t_permission` VALUES (2, 'user:add', '添加用户', 1, NULL);
INSERT INTO `t_permission` VALUES (3, 'user:delete', '删除用户', 1, NULL);
INSERT INTO `t_permission` VALUES (4, 'user:update', '编辑用户', 1, NULL);
INSERT INTO `t_permission` VALUES (5, 'user:select', '查询用户', 1, NULL);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `role_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色备注',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `uq_role_name`(`role_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 86 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, '超级管理员', '很牛逼的角色111');
INSERT INTO `t_role` VALUES (2, '业务员', '处理业务');
INSERT INTO `t_role` VALUES (78, '老板', '');
INSERT INTO `t_role` VALUES (83, '助理2', '帮老板做事，给老板送水');
INSERT INTO `t_role` VALUES (85, '123', '123');

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission`  (
  `role_id` int(0) NOT NULL COMMENT '角色id',
  `permission_id` int(0) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE,
  INDEX `fk_permission_id`(`permission_id`) USING BTREE,
  CONSTRAINT `fk_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`permission_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES (1, 1);
INSERT INTO `t_role_permission` VALUES (78, 1);
INSERT INTO `t_role_permission` VALUES (83, 1);
INSERT INTO `t_role_permission` VALUES (1, 2);
INSERT INTO `t_role_permission` VALUES (2, 2);
INSERT INTO `t_role_permission` VALUES (78, 2);
INSERT INTO `t_role_permission` VALUES (83, 2);
INSERT INTO `t_role_permission` VALUES (85, 2);
INSERT INTO `t_role_permission` VALUES (2, 3);
INSERT INTO `t_role_permission` VALUES (78, 3);
INSERT INTO `t_role_permission` VALUES (83, 3);
INSERT INTO `t_role_permission` VALUES (78, 4);
INSERT INTO `t_role_permission` VALUES (83, 4);
INSERT INTO `t_role_permission` VALUES (78, 5);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `user_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户的id',
  `user_acct` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `age` int(0) NULL DEFAULT NULL COMMENT '用户年龄',
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `gender` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户性别',
  `role` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '/用户对应角色/',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `uq_user_acct`(`user_acct`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (16, 'admin2', '123123', '测试', 23, '123123@qq.com', '18128283737', '男', NULL);
INSERT INTO `t_user` VALUES (18, 'test2', '123456', '平民', 18, 'agaga@13461.com', '1399451', '男', '管理员');
INSERT INTO `t_user` VALUES (22, 'haha', '123123', '123123', 11, '123123@qq.com', '18128283737', '女', NULL);
INSERT INTO `t_user` VALUES (50, 'test10', '123456', 'test10', 99, 'aasdgadsg@13216.com', '164649494', '男', NULL);
INSERT INTO `t_user` VALUES (54, 'zd1', '1122', 'zdzd', 13, 'aaa@111', '1331313', '男', '业务员1');
INSERT INTO `t_user` VALUES (57, 'zd2', '1122', 'zdzd', 13, 'aaa@111', '1331313', '男', NULL);
INSERT INTO `t_user` VALUES (59, 'zd4', '1122', 'zdzd', 13, 'aaa@111', '1331313', '男', NULL);
INSERT INTO `t_user` VALUES (223, 'a15', '123456', 'a15', 16, 'a145355dfa@6323.com', '19494965', '男', NULL);
INSERT INTO `t_user` VALUES (233, '测试用户角色关系1', '1122', 'zdzd', 13, 'aaa@111.com', '1331313', '男', NULL);
INSERT INTO `t_user` VALUES (235, '测试用户角色关系3', '1122', 'zdzd', 13, 'aaa@111', '1331313', '男', NULL);
INSERT INTO `t_user` VALUES (252, 'userAcct12', '123123', 'username12', 12, '12qq.com', '12312312', '女', NULL);
INSERT INTO `t_user` VALUES (259, 'userAcct19', '123123', 'username19', 19, '19qq.com', '12312319', '女', NULL);
INSERT INTO `t_user` VALUES (260, 'userAcct20', '123123', 'username20', 20, '20qq.com', '12312320', '女', NULL);
INSERT INTO `t_user` VALUES (262, 'userAcct22', '123123', 'username22', 22, '22qq.com', '12312322', '女', NULL);
INSERT INTO `t_user` VALUES (263, 'userAcct23', '123123', 'username23', 23, '23qq.com', '12312323', '女', NULL);
INSERT INTO `t_user` VALUES (264, 'userAcct24', '123123', 'username24', 24, '24qq.com', '12312324', '女', NULL);
INSERT INTO `t_user` VALUES (266, 'userAcct26', '123123', 'username26', 26, '26qq.com', '12312326', '女', NULL);
INSERT INTO `t_user` VALUES (267, 'userAcct27', '123123', 'username27', 27, '27qq.com', '12312327', '女', NULL);
INSERT INTO `t_user` VALUES (268, 'userAcct28', '123123', 'username28', 28, '28qq.com', '12312328', '女', NULL);
INSERT INTO `t_user` VALUES (273, 'userAcct33', '123123', 'username33', 33, '33qq.com', '12312333', '女', NULL);
INSERT INTO `t_user` VALUES (274, 'userAcct34', '123123', 'username34', 34, '34qq.com', '12312334', '女', NULL);
INSERT INTO `t_user` VALUES (275, 'userAcct35', '123123', 'username35', 35, '35qq.com', '12312335', '女', NULL);
INSERT INTO `t_user` VALUES (276, 'userAcct36', '123123', 'username36', 36, '36@qq.com', '12312336', '女', NULL);
INSERT INTO `t_user` VALUES (277, 'userAcct37', '123123', 'username37', 37, '37qq.com', '12312337', '女', NULL);
INSERT INTO `t_user` VALUES (278, 'userAcct38', '123123', 'username38', 38, '38qq.com', '12312338', '女', NULL);
INSERT INTO `t_user` VALUES (279, 'userAcct39', '123123', 'username39', 39, '39qq.com', '12312339', '女', NULL);
INSERT INTO `t_user` VALUES (290, 'test1', '123456', '2365465', 13, 'adfasdfas5@1213.com', '13213465498', '男', NULL);
INSERT INTO `t_user` VALUES (291, '123123123', '123123123', '123123123', 13213, '123123@qq.com', '123123213', '男', NULL);
INSERT INTO `t_user` VALUES (292, 'admin11', '123456', 'admin11', 15, 'aasdgasdg1532@13.com', '110', '男', NULL);
INSERT INTO `t_user` VALUES (295, 'ahahahaa', '123123123123', 'ahahahaa', 123, '123123@qq.com', '18128283737', '女', NULL);
INSERT INTO `t_user` VALUES (296, 'admin', '123456', 'admin', 23, '123456789@qq.com', '13812345678', '男', NULL);
INSERT INTO `t_user` VALUES (297, 'admin213', '123456', 'admin213', 23, '123456789@qq.com', '13812345678', '男', NULL);
INSERT INTO `t_user` VALUES (299, '2236715202', '123123123', '张三', 18, '2236715202@qq.com', '13987654321', '男', NULL);
INSERT INTO `t_user` VALUES (301, '东东哥', '123213', '东东哥', 123, '123123@qq.com', '18128283737', '男', NULL);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`  (
  `user_id` int(0) NOT NULL COMMENT '用户id',
  `role_id` int(0) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `fk_user_role_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `fk_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES (301, 1);
INSERT INTO `t_user_role` VALUES (301, 2);
INSERT INTO `t_user_role` VALUES (18, 83);
INSERT INTO `t_user_role` VALUES (295, 83);
INSERT INTO `t_user_role` VALUES (301, 83);

SET FOREIGN_KEY_CHECKS = 1;
